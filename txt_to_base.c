#include "txt_to_base.h"
#include <stdlib.h>
#include <stdio.h>
#include "sqlite3.h"

void initResultat(Resultat* re){
  an->tab = malloc(MAX_ITEM * sizeof(Item));

  an->nbItems = 0;
}

void loadItem(char *str, Item *it){
	char *next;
	it->proba = (float) strtod (str, &next);
	it->patience = (int) strtol (next, &next, 10);
	it->tempsPatience = (int) strtol (next, &next, 10);
	it->resultat = (float) strtod (next, NULL);

}

void ajouterItem(Resultat *re, Item *it){
  *(an->tab + an->nbItems) = *(it);
  an->nbItems++;

}

void chargerFichier(Resultat *re, FILE *f){

  char *str=NULL;
  size_t read;
  int i =0;

  Item it;
  while(getline(&str,&read,f) != -1){
	  i =0;
	  while (str[i] != '\n') i++;
	  str[i]='\0';
	  loadItem(str, &it);
	  ajouterItem(an, &it);
  }
  free(str);
  str = NULL;
}

void creerRequete (Item *it, char *requete){
	sprintf(requete, "insert into base(PROBA, PATIENCE, TEMPSSERVICE, RESULTAT) values (%f, %d, %d, %f);", it->proba, it->patience, it->tempsPatience, it->resultat);
}
void resultatToBase (Resultat *re, sqlite3 **db){
	int i, rc;
	char requete[500];
	char *zErrMsg = 0;
	FILE *f;
	for (i = 0; i < re->nbItems; i++){
		creerRequete(re->tab +i, requete);
		rc = sqlite3_exec(db, requete, 0, 0, &zErrMsg);
		
		if (rc != SQLITE_OK){
			f = fopen("error.log","a");
			fprintf(f, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
			fclose(f);
		}
	}
	
}