#ifndef _TXT_TO_BASE_H
#define _TXT_TO_BASE_H
#include <stdio.h>
#include "sqlite3.h"

#define MAX_ITEM 50

typedef struct {
	float proba;
	int patience;
	int tempsService;
	float resultat;
}Item;

typedef struct{
	Item *tab;
	unsigned int nbItem;
}Resultat;


void initResultat (Resultat *re);
void ajouterItem(Resultat *re, Item *it);
void loadItem(char *str, Item *it);
void chargerFichier (Resultat *re, FILE *f);
//Item creerItem (float proba, int patience, int tempsService, float resultat);
void creerRequete (Item *it, char *requete);
void resultatToBase (Resultat *re, sqlite3 **db);



#endif